#include <iostream>

using namespace std;

void convertidor(int x, float &resF){
    resF = ((float)x*9/5)+32; 

    return;
}

int main(){
    int gradosC = 0;
    float resF = 0;

    cout << "Ingrese la temperatura en Celcius: ";
    cin >> gradosC;

    convertidor(gradosC, resF);

    cout << "Los grados en Farenheit son: " << resF;
}