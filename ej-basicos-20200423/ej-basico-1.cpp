#include <iostream>

using namespace std;

char posineg(int x, char res){
    if (x > 0)
    {
        res = 'P';
    }else{
        res = 'N';
    }
    
    return res;
}

int main(){

    int numPN = 0;
    char res_posineg = ' ';

    cout << "Ingrese un numero para ver si es Positivo o Negativo: ";
    cin >> numPN ;

    res_posineg = posineg(numPN, res_posineg);

    cout << "El numero es: " << res_posineg;

    return 0;

}
