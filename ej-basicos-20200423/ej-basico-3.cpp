#include <iostream>

using namespace std;

void digito (char caracter, bool &respuesta){
    if (caracter >= 48 && caracter <= 57){
        respuesta = true;
    }
    return ;
}

int main(){
    char caracter = '\0';
    bool respuesta = false;

    cout << "Ingresar un caracter del teclado: ";
    cin >> caracter;

    digito(caracter, respuesta);

    if(respuesta == true){
        cout << "El caracter ingresado esta entre 0 y 9";
    }else{
        cout << "El caracter ingresado NO esta entre 0 y 9";
    }
}