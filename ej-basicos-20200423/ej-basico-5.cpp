#include <iostream>

using namespace std;
//5) Escribir una función que permita obtener el mayor de 2 números.
int BusqMayor( int x, int y, int resultado ){
    if (x < y)
    {
        resultado = y;
    }else{
        resultado = x;
    }
    return resultado;
}

int main(){
    int num1 = 0, num2 = 0, numMayor = 0;

    cout << "Ingrese el primer numero: ";
    cin >> num1;

    cout << "Ingrese el segundo numero: ";
    cin >> num2;

    numMayor = BusqMayor( num1, num2, numMayor );

    cout << "El numero mayor es: " << numMayor;
}