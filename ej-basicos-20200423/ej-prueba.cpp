#include <iostream>

using namespace std;

bool divisible( int x, int y, bool resp){
     if( x % y == 0){
        resp = true;
     }else{
        resp = false;
     }

    return resp;
}

int main (){
    int num1 = 0, num2 = 0;
    bool resul = false;

    cout << "Ingrese el primer numero: ";
    cin >> num1;

    cout << "Ingrese el segundo numero: ";
    cin >> num2;

    resul = divisible( num1 , num2, resul);

    if (resul == true){
        cout << "El primer valor ingresado es divisible por el segundo.";
    }else{
        cout << "El primer valor ingresado NO es divisible por el segundo.";
    } 
}