#include <iostream>

using namespace std;

void vocales( char caracter, bool &respuesta){
    switch (tolower(caracter))
    {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            respuesta = true;
            break;
        default:
            respuesta = false;
            break;
    }
    return;
}

int main(){
    char caracter = '\0';
    bool respuesta = false;

    cout << "Ingresar un caracter del teclado: ";
    cin >> caracter;
    
    vocales(caracter, respuesta);

    if(respuesta == true){
        cout << "El caracter ingresado es una Vocal";
    }else{
        cout << "El caracter ingresado NO es Vocal";
    }
    return 0;
}