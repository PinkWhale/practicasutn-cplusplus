#include <iostream>

using namespace std;

int factorial(int num){

    if( num == 0){
        return 1;
    }

    return num * factorial( num - 1) ;
}

float numE (int numero){
    
    if (numero == 0){
        return 1;
    }
    float resultado = 1 / float(factorial(numero));
    
    return resultado + numE(numero - 1);
}

int main(){
    int numeroUsuario = 0;

    cout << "Ingrese un numero para ser utilizado en la constante matematica E: ";
    cin >> numeroUsuario ;
    cout << "Constante matematica E del numero ingresado: " << numE(numeroUsuario) << endl;

    return 0;
}