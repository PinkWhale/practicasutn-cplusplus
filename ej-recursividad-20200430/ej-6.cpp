#include <iostream>

using namespace std;

int factorial(int n)
{   
    /*
    
    La regla de corte tendria que ser 1.
    Ya que cero factorial se define como 1,
    lo mismo sucede con 1 factorial, se define como 1.

    0!=1
    1!=1
    0!=1!

    */
    if (n == 1)
    {
        return 1;
    }

    /* 
    --n  es la decrementacion del valor de "n" antes de ser utilizado en la función
    osea que cuando sea evaluado su primera vez no va a valer 4 sino 3, y asi sucesivamente.
    */
    return n * factorial(n - 1);
}

int main()
{
    cout << factorial(4) << endl;
    return 0;
}