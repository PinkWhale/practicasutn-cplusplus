#include <iostream>

using namespace std;

int patron(int numero){

    if( numero == 0){
        return 0;
    }

    return numero + patron(numero - 1);
}

int main(){
    cout << patron(5) << endl;

    return 0;
}