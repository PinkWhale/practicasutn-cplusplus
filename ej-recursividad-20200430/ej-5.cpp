#include <iostream>

using namespace std;

void calcOctal(int num){
    if(num > 7){
        calcOctal(num / 8);
    }
    cout<< num % 8;

    return;
}

int main(){
    int numIngresado, resultOctal = 0;
    
    cout << "Ingresar numero decimal a convertir: ";
    cin >> numIngresado;

    cout << "El octal del numero es: " ;
    calcOctal(numIngresado); 

    return 0;
}