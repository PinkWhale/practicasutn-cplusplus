#include <iostream>

using namespace std;

int calcFibonacci(int num){
    if (num == 0){
        return 0;
    }else if(num == 1){
        return 1;
    }
    return calcFibonacci(num - 1) + calcFibonacci(num - 2);
}

int main(){
    int numIngresado = 0;

    cout << "Ingrese un numero: ";
    cin >>  numIngresado;
    
    cout <<"La serie Fibonacci es la siguiente: ";
    for(int i=0;i<numIngresado;i++){
        cout << calcFibonacci(i) << " ";
    }
    
    return 0;
}