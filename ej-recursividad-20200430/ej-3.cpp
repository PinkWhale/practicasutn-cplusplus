#include <iostream>

using namespace std;

void conjCollatz(int num){
    if (num > 1){
        //Si es par
        if ( num % 2 == 0){
            conjCollatz(num / 2);
        }else
        //Si es impar
        {
            conjCollatz((num * 3) + 1);
        }
    
    cout << num << " ";    
    }
    
    return;
}

int main (){
    int numero = 0;

    cout << "Ingrese un numero entero: ";
    cin >> numero;
    cout<< "Los numeros son los siguientes: ";
    conjCollatz(numero);

    return 0;
}