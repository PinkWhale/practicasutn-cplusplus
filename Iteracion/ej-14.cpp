//librerias
#include <iostream>

//cosa para decir standar
using namespace std;

//main class
int main(){

    int numI, acumulador_100, acumulador_m10, contador;
    float promedio;

    acumulador_100 = 0;
    acumulador_m10 = 0;
    contador = 0;
    promedio = 0;

    for (int i = 0; i < 5; i++)
    {
        cout << "Ingrese un numero: " << endl;
        cin >> numI;

        if (numI > 100)
        {
            acumulador_100 += numI;
            contador ++;
        }else if (numI < -10)
        {
            acumulador_m10 += numI;
        }
        
    };

    promedio = (float)acumulador_100 / contador;

    cout << "El promedio es " << promedio << " y la suma de numero menores a -10 es " << acumulador_m10 << "\n";
}