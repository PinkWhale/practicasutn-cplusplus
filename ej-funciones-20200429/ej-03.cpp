#include <iostream>

using namespace std;

void enviarMensaje(int &bateria){
    bateria = bateria - 1;
    return;
}

void realizarLlamada(int &bateria, int &minutosLlamada){
    bateria = bateria - (minutosLlamada * 0.08);
    return;
}

void tomarFoto(int &bateria){
    bateria = bateria - 6;
    return;
}

int main(){
    int minutosLlamada = 0;
    int bateria = 100;
    char accion = '\0';

    while (bateria > 0)
    {
        accion = true;

        cout << "Ingrese numero segun que quiere realizar(1- Enviar Mensaje, 2- Realizar llamada, 3- Tomar Foto): ";
        cin >> accion;
        
        switch (accion)
        {
            case '1':
                enviarMensaje(bateria);
                cout << "Bateria actual: " << bateria << endl;
                break;
            case '2':
                cout << "Ingrese los minutos de la llamada ";
                cin >> minutosLlamada;

                realizarLlamada(bateria, minutosLlamada);
                cout << "Bateria actual: " << bateria << endl;
                break;
            case '3':
                tomarFoto(bateria);
                cout << "Bateria actual: " << bateria << endl;
                break;
            default:
                cout << "No es una opcion Valida..." << endl ;
                cout << "Bateria actual: " << bateria << endl;
            break;
        }
    }

}