//librerias
#include <iostream>
#include <bits/stdc++.h>

using namespace std;

void dibujo(string figura, char pixel, int tamanio){
    if (figura == "cuadrado")
    {
        for (int fila=0; fila<tamanio ; ++fila) {
            for (int colum=0; colum<tamanio ; ++colum) {
            cout << pixel ;
            }
        cout << endl;
        }

    }else if(figura == "triangulo"){
        for (int fila = 0; fila < tamanio; ++fila)
        {
            for (int columna = 0; columna <= fila; ++columna)
            {
                cout << pixel ;
            }
            cout << endl;
            
        }
        
    }else{
        cout << "No es una figura posible de dibujar.";
    }
return;
}


int main(){

    string figura =" ";
    int tamanio = 0;
    char pixel = '+';

    cout << "Escribir que desea dibujar (cuadrado/triangulo): ";
    cin >> figura ;
    transform(figura.begin(), figura.end(), figura.begin(), ::tolower); 
    cout << "Ingresar largo de lado o base: ";
    cin >> tamanio;

    dibujo(figura, pixel, tamanio);
    
return 0;  
    
}